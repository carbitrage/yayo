import { connection } from "websocket";

interface MarketData {
    pair: string;
    markets: string[];
}

interface Connection extends connection {
    data: MarketData;
}

declare interface PriceData {
    pair: string;
    markets: string[];
    prices: number[];
}

declare interface QueryRow {
    pair: string;
    market: string;
    price: number;
}

declare type DataEntry = {
    id: string;
    label: string;
    value: number;
    color: string;
}