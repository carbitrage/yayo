# Yayo

Yayo is a **WebSocket** server that provides hot data (dynamically updated prices) for frontend part (Hollywood project).

## Installation
Clone current repository and install required packages via `npm install`. After that, execute `npm run build` to compile TypeScript into JavaScript.

After build, configure environment variables and then execute `npm start`.

## Configuration
Make a copy of `.env.example` file and rename it to `.env` and then open it.

### Environment variables
- **SOCKET_HOST** - defines what network interface you want to use to start your server. It's **0.0.0.0** by default so Node.js would start listening using IPv4 interface.
- **SOCKET_PORT** - network port.
- **DB_HOST** - network hostname (or IP address) of your database location.
- **DB_PORT** - network port.
- **DB_NAME** - database name.
- **DB_USER** - database user.
- **DB_PASSWORD** - database password.