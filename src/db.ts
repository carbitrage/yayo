import { Pool, PoolClient, QueryResult } from "pg";
import { QueryRow } from "../types/data";
import 'dotenv';

async function createPool(): Promise<Pool> {
    return new Promise<Pool>(resolve => {
        const pool: Pool = new Pool(
            {
                user: process.env.DB_USER, 
                host: process.env.DB_HOST, 
                port: +process.env.DB_PORT!, 
                database: process.env.DB_NAME, 
                password: process.env.DB_PASSWORD
            }
        );
        resolve(pool);
    });
}

async function getResponse(pool: Pool): Promise<void> {
    const query: string = 'SELECT pair, market, price FROM pairs_and_markets';
    while(true) {
        await new Promise<QueryRow[]>(async resolve => {
            pool.connect(async (err: Error, client: PoolClient, release: any) => {
                await client.query(query)
                    .then((res: QueryResult) => {
                        process.env.DATA = JSON.stringify(res.rows as QueryRow[]);
                    })
                    .catch((err: Error) => {
                        console.error(`Database error: ${err.stack}`);
                    });
                release();
                resolve();
            });
        });
        await new Promise(resolve => setTimeout(resolve, 3000));
    }
}

async function prepareData(): Promise<void> {
    return new Promise(async resolve => {
        const pool = await createPool();
        await getResponse(pool);
        resolve();
    });
}

export { prepareData };