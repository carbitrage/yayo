import http, { IncomingMessage, ServerResponse } from 'http';
import { server as websocketServer, request, IMessage, IStringified } from 'websocket';
import { Connection, MarketData, DataEntry } from '../types/data';
import { prepareSendData } from './processor';
import 'dotenv';

function startServer() {
    const server: http.Server = http.createServer(function(request: IncomingMessage, response: ServerResponse) {
        console.log(`${new Date()} Received request for ${request.url}`);
        response.writeHead(404);
        response.end();
    });

    const serverPort: number = +(process.env.SOCKET_PORT as string);
    const serverHost: string = process.env.SOCKET_HOST as string;

    server.listen(serverPort, serverHost, function() {
        console.log(`${new Date()} Server is listening on port ${serverPort}`);
    });

    const wsServer: websocketServer = new websocketServer({
        httpServer: server,
        autoAcceptConnections: false
    });

    function originIsAllowed(origin: string) {
        return true;
    }

    wsServer.on('request', function(request: request) {
        if (!originIsAllowed(request.origin)) {
            request.reject();
            console.log(`${new Date()} Connection from origin ${request.origin} rejected`);
            return;
        }
        const connection = request.accept() as Connection;
        console.log(`${new Date()} Connection accepted`);
        connection.on('message', async function(message: IMessage) {
            if (message.type === 'utf8' && message.utf8Data) {
                connection.data = JSON.parse(message.utf8Data) as MarketData;
                const currentData: MarketData = connection.data;
                console.log(`Received message: ${message.utf8Data}`);
                while (currentData === connection.data && connection.data.markets.length > 0) {
                    const queryResult: DataEntry[] = await prepareSendData(connection.data);
                    connection.sendUTF(JSON.stringify(queryResult) as IStringified);
                    await new Promise(resolve => setTimeout(resolve, 3000));
                }
            }
            else if (message.type === 'binary' && message.binaryData) {
                console.log(`Received Binary Message of ${message.binaryData.length} bytes`);
                connection.sendBytes(message.binaryData as Buffer);
            }
        });
        connection.on('close', function(reasonCode: number, description: string) {
            console.log(`${new Date()} Peer ${connection.remoteAddress} disconnected, reason code: '${reasonCode}', description: '${description}'`);
        });
    });
}

export { startServer };